package com.bhas.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/security")
@Slf4j
public class EmployeeController 
{
	
	@GetMapping("/test")
	@ResponseStatus(code = HttpStatus.INSUFFICIENT_STORAGE)
	public String greetMsg()
	{
		log.info("@Sl4j");
		return "Welcome to the Spring Security Application demo project";
	}
}
