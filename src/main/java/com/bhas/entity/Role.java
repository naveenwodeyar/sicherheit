package com.bhas.entity;

public enum Role
{
	USER,
	ADMIN,
	GUEST
}
