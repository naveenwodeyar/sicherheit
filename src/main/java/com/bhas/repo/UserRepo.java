package com.bhas.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.bhas.entity.User;

@Repository
public interface UserRepo extends JpaRepository<User, Integer> 
{

}
